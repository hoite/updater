#!/bin/bash
# AUTHOR: Hoite Prins
# Kansloos update script met een teller voor de yolo

# Check of het script als root wordt uitgevoerd
if (( $EUID != 0 )); then
    echo "Je moet m als root uitvoeren pannekoek"
    exit
fi

echo $1

if [[ "$1" = "teller" ]]; then

	for ((i = 1; i<= $2; i++ ));
	do
		echo $i

	done
	exit
fi


# check of het eerste argument update of upgrade is
if [[	"$1" = "update"  ||  "$1" = "upgrade" ]]; then

	if [[ "$1" = "update" ]]; then
		apt-get update
		echo "repo geupdate"
		exit

	elif [[ "$1" = "upgrade" ]]; then
		apt-get update
		apt-get upgrade
		echo "Het systeem is geupgrade"
		exit
	fi

else
	echo "Voer het script uit als ./updater.sh update of ./updater.sh upgrade"

fi
